#ifndef POSITION_H
#define POSITION_H

#include <string>
#include <iostream>


class Position{
	private :
	int x ; 
	int y ;
	
	public :
	Position(int, int);
	Position();
	~Position();
	
	int getX();
	int getY();
	
	void setX(int);
	void setY(int);	
	void afficher();
	bool operator==(Position &p);
	bool operator!=(Position &p);
	Position &operator=(Position &p);
	
};

#endif
