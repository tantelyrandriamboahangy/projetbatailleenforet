LIBS = -lsfml-graphics -lsfml-window -lsfml-system

all : main
	./main

main : Terrain.o Position.o Element.o Fenetre.o main.o 
	echo $^ 
	g++  $(LIBS) $^ -o main

main.o : main.cc 
	@g++ -Wall -c main.cc 

Terrain.o : Terrain.cc Terrain.h
	@g++ -Wall -c Terrain.cc
	 
Position.o : Position.cc Position.h
	@g++ -Wall -c Position.cc

Element.o : Element.cc Element.h
	@g++ -Wall -c  Element.cc
	
Fenetre.o : Fenetre.cc Fenetre.h
	@g++ -Wall -c Fenetre.cc

install :
	sudo apt-get install libsfml-dev

clean :
	rm -rf *.o
	rm -rf main    
	rm -rf *.gch 

