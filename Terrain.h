#ifndef TERRAIN_H
#define TERRAIN_H
#include <string>
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "Element.h"

#define NB_CASE_MAX 10
#define NB_ELEM_MAX 20

class Terrain {
	private : 
	std::vector< Element* > Elements ;
	
	public :
	Terrain();
	Terrain(std::string);
	~Terrain();
	
	void addElement(int vie,int x,int y,typeElement t,Hauteur h);
	void Sauvegarder(std::string fichier);
	void Lire_sauvegarde(std::string svg) ;	
	void afficheVector();
	void dessineElements(sf::RenderWindow&);
};

#endif
