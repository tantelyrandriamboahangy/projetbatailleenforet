#include "Position.h"

using namespace std ;

Position::Position(int xp, int yp){
	x=xp;
	y=yp;
}
Position::Position(){x=0; y=0;}

Position::~Position(){
//~ cout<<"Destruction position"<<endl;
};
	
int Position::getX(){
	return x;
}

int Position::getY(){
	return y ;
}
	
void Position::setX(int xp){ 
	x=xp; 
}

void Position::setY(int yp){
	y=yp;
}	

void Position::afficher(){
	cout<<"|Position ("<<x<<","<<y<<")"<<endl;
}

bool Position::operator==(Position &p) {
	return (p.x == x && p.y == y);
}
bool Position::operator!=(Position &p) {
	return !(*this == p);
}

Position &Position::operator=(Position &p) {
	if( *this != p){ 
		x=p.x ; y=p.y;
		return p ;
	}
	else return *this ;
}






