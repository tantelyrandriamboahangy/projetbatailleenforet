#include "Element.h"
#include <iostream>


using namespace std;

	Element::Element(){
	pos.setX(0); pos.setY(0);
	val_vie=100;
	hauteur=Bas;
	init_diametre();
	typeElem=Null;
	}
	
	
	Element::Element(int vie,int x,int y,typeElement t,Hauteur h){
	pos.setX(x); pos.setY(y);
	val_vie=vie;
	hauteur=h;
	init_diametre();
	typeElem=t;
	}
	
	Element::~Element(){
	cout<<"destruction Element\n";
	}
	
	void Element::afficher(){
		std::string tmp;
		if(typeElem == Arbre)tmp="Arbre";
		if(typeElem == Rocher)tmp="Rocher";
		if(typeElem == Buisson)tmp="Buisson";
		cout<<"Element =>" << "Vie: "<<val_vie
		<<" |Type: "<<tmp<<" |Hauteur: "<<hauteur<<" |Diametre: "<<diametre<<" ";
		pos.afficher();
	}
	
	//calcul le diametre en fonction de la hauteur de l'element
	void Element::init_diametre(){
		if( hauteur == Bas ) diametre = 10 ;
		else if( hauteur == Moyen ) diametre = 15;
		else if( hauteur == Elevee ) diametre = 20 ;
	}
	
	typeElement Element::getType(){
		return typeElem;
	}
	
	int Element::getVie(){
		return val_vie;
	}
	
	Hauteur Element::getHauteur(){
		return hauteur;
	}
	
	Position& Element::getPos(){
		return pos;
	}
	
	int Element::getDiametre(){
		return diametre;
	}
	
	void Element::setTypeElem(typeElement t){
		typeElem = t;
	}
	
	void Element::setVie(int v){
		val_vie = v; 
	}
	
	void Element::PrendDegat(int deg){
		val_vie -= deg ;
	}
	
// A deplacer dans une sous class d'Element pouvent se déplacer
// dans le terrain (un personnage)
	void Element::setpos(int xp , int yp ){
		pos.setX(xp);
		pos.setY(yp);
	}
	
	
	// à modifier pour la taille et aussi l'image à charger 
	void Element::dessine(sf::RenderWindow &window){
		//~ sf::Texture  t;
		//~ if(!t.loadFromFile("images/tree1.png"))cout<<"Erreur image\n\n";

		//~ sf::Sprite sprite(t);
		//~ sprite.setPosition(pos.getX(),pos.getY());
		//~ sprite.setScale(0.05,0.05);
		//~ window.draw(sprite);
		sf::CircleShape shape(getDiametre());
		shape.setFillColor(sf::Color(0,128,0));
		shape.setPosition(pos.getX(),pos.getY());
		 window.draw(shape);
	}

	

