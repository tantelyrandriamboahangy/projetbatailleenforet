#ifndef FENETRE_H
#define FENETRE_H
#include <string.h>
#include <iostream>
#include <SFML/Graphics.hpp>

#define case_size 20
#define nb_col 48
#define nb_lg  24
#define Width 900
#define Height 600



class Fenetre{
private :
	int H ;
	int W ;
	std::string nomFenetre ;
	void affichage_principale(std::string);
	
public :
	Fenetre(std::string);
	~Fenetre();
	
	void  event_close_windows(sf::RenderWindow &window);
	void lancer_jeu();
	
};
#endif
