    #include "Fenetre.h"
#include "Position.h"
#include "Terrain.h"



using namespace sf ; 
using namespace std ; 

    
//Centre la position d'un ELement dans un case 
void  Centrer_position(Position& p){
	int indiceX= (p.getX() - 225) / case_size;
	int indiceY= p.getY() / case_size ;
	
	p.setX( (indiceX * case_size) + 225);
	p.setY( (indiceY * case_size));
	cout<<p.getX()<<" "<<p.getY()<<"\n";
}

//affiche un quadrillage pour l'editeur de niveau à la postion x,y
void affiche_quadrillage(int taille_case,int x, int y , RenderWindow &window){
	
	RectangleShape  rect( Vector2f(Width*0.75,Height ));
    rect.setFillColor(sf::Color(194,230,123));
    rect.setPosition(x,y);
    window.draw(rect);
	for(int i = 1 ; i < Width/taille_case  ; i ++) {
			sf::Vertex l[] =
		{
			sf::Vertex(sf::Vector2f(x+(i*taille_case),y)),
			sf::Vertex(sf::Vector2f(x+(i*taille_case),y+Height))
		};
		window.draw(l, 2, sf::Lines);
	}
	for(int i = 1 ; i < Height/taille_case  ; i ++) {
			sf::Vertex l[] =
		{
			sf::Vertex(sf::Vector2f(x,y+(i*taille_case))),
			sf::Vertex(sf::Vector2f(Width, y+(i*taille_case)))
		};
		window.draw(l, 2, sf::Lines);
	}
}

//Verifie si le clic n'est pas en dehors de notre fenetre 
bool ClicEstDansFenetre(Position p){
	if( p.getX() > 0 && p.getX() < Width && p.getY() < Height && p.getY() > 0) return true ;
	else return false ;
}

bool ClicEstDansFenetre(int x , int y){
	if( x > 225 && x < Width && y < Height && y > 0) return true ;
	else return false ;
}

//Le processus actuel attend pendant un temps t ( en millisecond )
void attendre(double t){
	sf::sleep(sf::milliseconds(t));
}


// attribue dans une position les coordonnees du clic (x,y) 
// attend 250 millisecondes après le clic 
void getclic(sf::RenderWindow &window,Position &pos){
	if (Mouse::isButtonPressed(Mouse::Left))
	{
		Vector2i tmpVect = Mouse::getPosition(window);
		
		if( ClicEstDansFenetre(tmpVect.x,tmpVect.y) ){
		pos.setX(tmpVect.x);
		pos.setY(tmpVect.y);
		cout<<"Clic souris gauche ok :"<<pos.getX()<<" "<<pos.getY()<<"\n";
		}

		attendre(250);
	}

}



void editeur_niveau(Terrain &t, RenderWindow &window){
	Position ps;
	
	//à remplacer par l'image du l'element en question 
	RectangleShape  rect( Vector2f(20,20));
    rect.setFillColor(sf::Color(0,128,0));
    rect.setPosition(10,100);
    window.draw(rect);
    
	RectangleShape  rect1( Vector2f(15,15));
    rect1.setFillColor(sf::Color(0,150,0));
    rect1.setPosition(45,100);
    window.draw(rect1);
    
	RectangleShape  rect2( Vector2f(10,10));
    rect2.setFillColor(sf::Color(0,175,0));
    rect2.setPosition(75,100);
    window.draw(rect2);

	 
	getclic(window,ps);
	if( ClicEstDansFenetre(ps) ){
		Centrer_position(ps);
		t.addElement(100,ps.getX(),ps.getY(),Arbre,Bas);
		t.afficheVector();
	}
	affiche_quadrillage(case_size,Width-(Width*0.75),0,window);
	t.dessineElements(window);
	

}

Fenetre::Fenetre(std::string title){
	H = Height;
	W = Width;
	nomFenetre = title ;
}


Fenetre::~Fenetre(){}

//on appelle ici toutes les methodes/fonctions d'affichage.
void Fenetre::lancer_jeu(){
	
	RenderWindow window(VideoMode(W,H),nomFenetre ,Style::Close);
	window.setFramerateLimit(100);
	Terrain t(".save");
	Position ps;

	//event à faire tant que la fenetre est ouverte 
    while (window.isOpen())
    {
		event_close_windows(window);
        window.clear();
        //afficha
		editeur_niveau(t,window);
		//
        window.display();
	}

}
		
	//gestion fermeture fenetre
void  Fenetre::event_close_windows(sf::RenderWindow &window){
	sf::Event event;
    while (window.pollEvent(event))
     {
       // fermeture de la fenêtre lorsque l'utilisateur le souhaite
       if (event.type == sf::Event::Closed)
                window.close();
     }
		
}
	


//~ void Fenetre::dessine_arbre(int x, int y,RenderWindow &window){
	//~ sf::Texture  t;
	//~ if(!t.loadFromFile("images/tree1.png"))cout<<"Erreur image\n\n";

	//~ sf::Sprite sprite(t);
	//~ sprite.setPosition(300,100);
	//~ sprite.setScale(0.10,0.10);
	//~ window.draw(sprite);
	
//~ }










