#ifndef ELEMENT_H
#define ELEMENT_H

#include <string>
#include "Position.h"
#include <SFML/Graphics.hpp>


enum typeElement { Null=0,Arbre=1,Rocher=2,Buisson=3};
enum Hauteur {Bas=1,Moyen=2,Elevee=3};


class Element {
	private :
		Position pos;
		typeElement typeElem; 
		int val_vie;
		Hauteur hauteur; 
		int diametre ;
	
	public:
		Element();	
		Element(int,int,int,typeElement,Hauteur);
		~Element();		
		void afficher();
		void init_diametre();
		typeElement getType();
		int getVie();
		Position& getPos();
		Hauteur getHauteur();
		int getDiametre();
		void setTypeElem(typeElement);
		void setVie(int);
		void PrendDegat(int);
		void setpos(int,int);
		void dessine(sf::RenderWindow&);
};


#endif 
