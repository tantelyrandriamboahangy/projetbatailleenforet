#include "Terrain.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <stdio.h>


using namespace std ;

Terrain::Terrain(){

}
	
//init la matrice depuis la sauvegarde
Terrain::Terrain(string svg){
	Lire_sauvegarde(svg);
	afficheVector();
}
	
	
Terrain::~Terrain(){
	//~ cout<<"Appel destructeur terrain\n";
}

void Terrain::addElement(int vie,int x,int y,typeElement t,Hauteur h){
	Elements.push_back( new Element(vie,x,y,t,h) );
	Sauvegarder(".save");
}
	
void Terrain::Sauvegarder(std::string nom_fichier){
        ofstream fichier(nom_fichier, ios::out);
        
 
        if(fichier)
        {
			for(unsigned int i= 0; i<Elements.size(); i++ )	
			fichier<<Elements[i]->getVie()<<" "
				   <<Elements[i]->getPos().getX()<<" "
				   <<Elements[i]->getPos().getY()<<" "
				   <<Elements[i]->getType()<<" "
				   <<Elements[i]->getHauteur()<<"\n";
            fichier.close();
        }
        else
               cerr << "Impossible d'ouvrir le fichier !" << endl;
	}
	
void Terrain::Lire_sauvegarde(string svg){
FILE *fichier = fopen(".save","r");
	if(fichier){
		int p[5];
		typeElement t;
		Hauteur h;
		int ret;
		ret = fscanf(fichier,"%d %d %d %d %d",&p[0],&p[1],&p[2],&p[3],&p[4]) ;
		while(ret>0){
		if( p[3] == Arbre) t = Arbre; 
			else if( p[3] == Rocher) t = Rocher;
				else t=Buisson;
		if( p[4] == Bas) h=Bas;
			else if( p[4] == Moyen) h=Moyen;
				else p[4]=Elevee;
		Elements.push_back(new Element(p[0],p[1],p[2],t,h));
		ret = fscanf(fichier,"%d %d %d %d %d",&p[0],&p[1],&p[2],&p[3],&p[4]) ;
		}
	} 
	else 
	cerr << "Impossible de lire la sauvegarde" << endl;		
}


void Terrain::afficheVector(){
	
	if( Elements.empty() ) return ;
	else 
	{
		for(unsigned int i= 0; i<Elements.size(); i++ ){Elements[i]->afficher();}
	}
}

//dessine tous les Elements contenu dans le Vector Elements
void Terrain::dessineElements(sf::RenderWindow& window){
	if ( !Elements.empty() ) {
		for(unsigned int i=0 ; i< Elements.size();i++){
				Elements[i]->dessine(window);
		}
	}
}



